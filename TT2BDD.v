Require Import String.
Require Import List.      (* sequence *)
Require Import Multiset.  (* bag *)
Require Import ListSet.   (* set *)
Require Import Bool.

Require Import TT.
Require Import BDD.

(*
    - Helper for GetPortValueOfRow
    - Return the value of a port in a list of cell
 *)
Fixpoint GetPortValueInListOfCells (l: list Cell) (port: Port): Value :=
    match l with
    | BuildCell p v::rest => if (port_eq p port) then v else (GetPortValueInListOfCells rest port)
    | nil => False
    end.

(*
    - Return the value of a port in a given row
*)
Definition GetPortValueOfRow (r: Row) (p: Port) : Value :=
    match r with BuildRow l => GetPortValueInListOfCells l p 
end.

(*
    - Return true if port is defined for all rows in a list of row
*)
Fixpoint IsPortDefinedForAllRows (rows: list Row) (port: Port) : bool :=
    match rows with 
        | row::rest => if (value_eq (GetPortValueOfRow row port) Undefined) then false else (IsPortDefinedForAllRows rest port)
        | nil => true
    end.

(*
    - If there is a port defined for all rows return this port, else return None
*)
Fixpoint GetBestPort (rows: list Row) (usablePorts : list Port) : option Port :=
    match usablePorts with
        | port::rest =>  if (IsPortDefinedForAllRows rows port) then Some port else (GetBestPort rows rest)
        | nil => None
    end.

(*
    - Return the list of rows where the given port is False
*)
Fixpoint GetZeroPart (rows: list Row) (port: Port) : list Row :=
    match rows with 
        | row::rest =>
            match GetPortValueOfRow row port with
                | False => row::(GetZeroPart rest port)
                | _ => GetZeroPart rest port
            end
        | nil => nil
    end.

(*
    - Return the list of rows where the given port is True
*)
Fixpoint GetOnePart (rows: list Row) (port: Port) : list Row :=
        match rows with 
            | row::rest =>
                match GetPortValueOfRow row port with
                    | True => row::(GetOnePart rest port)
                    | _ => GetOnePart rest port
                end
            | nil => nil
        end.

(*
    - Return a minimal tree from a list of rows, input ports and output port
*)
Fixpoint GetMinimalTree (rows: list Row) (usablePorts : list Port) (outputPort : Port) (n: nat) : Tree :=
    (* n represents the length of the usablePorts list, it helps Coq to figure out that our recursion is well formed *)
    match n with
    (* If we arrive there it means we have a line in double, so we print that there is an issue*)
    | 0 => BuildLeaf(BuildAssignement (BuildPort "MALFORMED TABLE, DUPLICATE LINE") Undefined)
    | S u =>
        (* We find the port which is defined for every row *)
        match GetBestPort rows usablePorts with
            | Some port => 
                (* We remove the port from the list *)
                match remove port_eq port usablePorts with updatedPorts =>
                    (* We create the tree recursively *)
                    match GetZeroPart rows port with
                        | elZ::nil =>
                            match GetOnePart rows port with
                                | elO::nil => BuildSubTree port (BuildLeaf (BuildAssignement outputPort (GetPortValueOfRow elZ outputPort))) (BuildLeaf (BuildAssignement outputPort (GetPortValueOfRow elO outputPort)))
                                | oP => BuildSubTree port (BuildLeaf (BuildAssignement outputPort (GetPortValueOfRow elZ outputPort))) (GetMinimalTree oP updatedPorts outputPort u)
                            end
                        | zP =>
                            match GetOnePart rows port with
                                | elO::nil => BuildSubTree port (GetMinimalTree zP updatedPorts outputPort u) (BuildLeaf (BuildAssignement outputPort (GetPortValueOfRow elO outputPort)))
                                | oP => BuildSubTree port (GetMinimalTree zP updatedPorts outputPort u) (GetMinimalTree oP updatedPorts outputPort u)
                            end
                    end
                end
            (*If we arrive here it means the table is malformed and we can't build a tree from it*)
            | None => BuildLeaf(BuildAssignement (BuildPort "MALFORMED TABLE, NOT ENOUGH PORTS DEFINED") Undefined)
        end
    end.

(*
    - Build a min BDD from a TruthTable
*)
Definition TT2BDDMin (truthTable: TT) : Tree :=
        match truthTable with BuildTT inputPorts outputPort rows => GetMinimalTree rows inputPorts outputPort (length inputPorts)
    end.


(*
    - Example taken from https://www.transformation-tool-contest.eu/2019/2019-tt2bdd.pdf
*)
Definition a: Port :=
    BuildPort "a".

Definition b: Port :=
    BuildPort "b".

Definition c: Port :=
    BuildPort "c".

Definition d: Port :=
    BuildPort "d".

Definition s: Port :=
    BuildPort "s".

Definition t1Table: TT :=
    BuildTT (a::b::c::d::nil) s
    (BuildRow
        (BuildCell a False::
        BuildCell b False::
        BuildCell c Undefined::
        BuildCell d Undefined::
        BuildCell s False::
        nil)
    ::BuildRow
        (BuildCell a False::
        BuildCell b True::
        BuildCell c False::
        BuildCell d False::
        BuildCell s True::
        nil)
    ::BuildRow
        (BuildCell a False::
        BuildCell b True::
        BuildCell c False::
        BuildCell d True::
        BuildCell s False::
        nil)
    ::BuildRow
        (BuildCell a False::
        BuildCell b True::
        BuildCell c True::
        BuildCell d Undefined::
        BuildCell s False::
        nil)
    ::BuildRow
        (BuildCell a True::
        BuildCell b False::
        BuildCell c False::
        BuildCell d False::
        BuildCell s False::
        nil)
    ::BuildRow
        (BuildCell a True::
        BuildCell b False::
        BuildCell c True::
        BuildCell d False::
        BuildCell s True::
        nil)
    ::BuildRow
        (BuildCell a True::
        BuildCell b Undefined::
        BuildCell c Undefined::
        BuildCell d True::
        BuildCell s False::
        nil)
    ::BuildRow
        (BuildCell a True::
        BuildCell b True::
        BuildCell c False::
        BuildCell d False::
        BuildCell s True::
        nil)
    ::BuildRow
        (BuildCell a True::
        BuildCell b True::
        BuildCell c True::
        BuildCell d False::
        BuildCell s False::
        nil)
    ::nil).


Compute TT2BDDMin t1Table. 
    

