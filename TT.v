Require Import List.      (* sequence *)
Require Import Multiset.  (* bag *)
Require Import ListSet.   (* set *)
Require Import Bool.
Require Import String.
Open Scope string.

(* Represents Value *)
Inductive Value:=
    | True : Value
    | False : Value
    | Undefined : Value.

(* Defining equality for value *)
Definition value_eq : forall (x y : Value), { x = y } + { x <> y }.
Proof.
decide equality.
Defined.

(* Represents input or output port of a table/a tree*)
Inductive Port:=
    BuildPort:
        (*name*) string -> Port.

(* Represents a cell of a table *)
Inductive Cell:=
    BuildCell:
        (*port*) Port ->
        Value -> Cell.

(* Represents a row of a table*)
Inductive Row:=
    BuildRow:
        (*cells*) list Cell -> Row.

(* Represents a truth table *)
Inductive TT:=
    BuildTT:
        (*input ports*) list Port ->
        (*output ports*) Port ->
        (*rows*) list Row -> TT.

(* Defining equality between ports based on port name *)
Definition port_eq : forall (x y : Port), {x = y} + {x <> y}.
Proof.
decide equality.
apply string_dec.
Defined.