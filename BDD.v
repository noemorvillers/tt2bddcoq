Require Import TT.
Require Import List.      (* sequence *)
Require Import Multiset.  (* bag *)
Require Import ListSet.   (* set *)
Require Import Bool.
Require Import String.

(*
    - Represent an output port and value assigned to it
    - We separated Leaf and Assignement because we wanted to be able to manage multiple outputs ports (didnt have time for it tho)
*)
Inductive Assignement : Set :=
    BuildAssignement:
        (*port*) Port ->
        (*value*) Value -> Assignement.

(*
    - Represents a tree
*)
Inductive Tree : Set :=
    | BuildLeaf:
        (*assignement*) Assignement -> Tree
    | BuildSubTree:
        (*port*) Port ->
        (*treeForZero*) Tree ->
        (*treeForOne*) Tree -> Tree.
